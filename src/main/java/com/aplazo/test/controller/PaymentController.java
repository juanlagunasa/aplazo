package com.aplazo.test.controller;

import com.aplazo.test.dto.PaymentRequest;
import com.aplazo.test.interfaces.FunctionCalculate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = "/v1")
public class PaymentController {

    @Autowired
    private FunctionCalculate functionCalculate;

    @PostMapping(value="/calculate")
    public ResponseEntity calculatePayment(@RequestBody @Valid PaymentRequest paymentRequest){
        log.info("request {}",paymentRequest.toString());
        return new ResponseEntity(functionCalculate.calculate(paymentRequest), HttpStatus.CREATED);
    }

}
