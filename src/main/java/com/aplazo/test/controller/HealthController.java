package com.aplazo.test.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1")
public class HealthController {

    @GetMapping(value = "/health", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity health() {

        return ResponseEntity.ok("{\"state\":\"up\"}");
    }
}
