package com.aplazo.test.exception;

import com.aplazo.test.dto.ResponseException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.SimpleDateFormat;
import java.util.Date;

@ControllerAdvice
public class MethodArgumentNotValidExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseException methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return processFieldErrors(ex);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ResponseException exception(Exception ex) {
        return errors(ex);
    }

    private ResponseException processFieldErrors(MethodArgumentNotValidException ex) {
        StringBuffer sb = new StringBuffer();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            sb.append(error.getDefaultMessage());
            sb.append(" ");
        });
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return ResponseException.builder().cod(1).msg(sb.toString().trim()).salt(formateador.format(ahora)).build();

    }

    private ResponseException errors(Exception ex) {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return ResponseException.builder().cod(100).msg(ex.getMessage()).salt(formateador.format(ahora)).build();

    }

}
