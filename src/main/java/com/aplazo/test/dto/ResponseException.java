package com.aplazo.test.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseException {

    private int cod;
    private String msg;
    private String salt;

}
