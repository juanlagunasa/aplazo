package com.aplazo.test.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Data
@Builder
public class PaymentRequest {

    @Min(value = 1, message = "The amount should be more than $1.00, the max should be lesser than $999,999.00")
    @Max(value = 999999, message = "The amount should be more than $1.00, the max should be lesser than $999,999.00")
    private BigDecimal amount;

    @Min(value = 4, message = "The max terms (weeks) were the payment can be paid is 52, the minimum should be 4.")
    @Max(value = 52, message = "The max terms (weeks) were the payment can be paid is 52, the minimum should be 4.")
    private int terms;

    @Min(value = 1, message = "The rate should bigger than 1%, lesser than 100%.")
    @Max(value = 100, message = "The rate should bigger than 1%, lesser than 100%.")
    private double rate;

}
