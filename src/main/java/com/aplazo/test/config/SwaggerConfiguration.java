package com.aplazo.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(swaggerConfiguration()).select()
                .apis(RequestHandlerSelectors.basePackage("com.aplazo.test.controller"))
                .paths(PathSelectors.any()).build();
    }

    private ApiInfo swaggerConfiguration() {
        return new ApiInfoBuilder().title("Test Rest doc").version("1.0").license("Apache License Version 2.0")
                .build();
    }
}
