package com.aplazo.test.service;

import com.aplazo.test.dto.PaymentRequest;
import com.aplazo.test.dto.PaymentResponse;
import com.aplazo.test.interfaces.FunctionCalculate;
import com.aplazo.test.utils.Util;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalculatePaymentService implements FunctionCalculate {

    @Override
    public List<PaymentResponse> calculate(PaymentRequest paymentRequest){
        double amountTot = ((paymentRequest.getAmount().doubleValue()*paymentRequest.getRate())/100)+paymentRequest.getAmount().doubleValue();
        return Util.calculateDaysDeadLines(amountTot, paymentRequest.getTerms());
    }
}
