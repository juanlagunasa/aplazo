package com.aplazo.test.interfaces;

import com.aplazo.test.dto.PaymentRequest;
import com.aplazo.test.dto.PaymentResponse;

import java.util.List;

public interface FunctionCalculate {

    List<PaymentResponse> calculate(PaymentRequest paymentRequest);
}
