package com.aplazo.test.utils;

import com.aplazo.test.dto.PaymentResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Util {

    /**TODO calculate to holidays*/

    public static List<PaymentResponse> calculateDaysDeadLines(double amount, int terms){
        List<PaymentResponse> listPayments = new ArrayList<>();
        LocalDate date = LocalDate.now();
        for (int i = 1; i <= terms; i++) {
            LocalDate newDate = date.plusDays(7 *i);
            listPayments.add(
                    PaymentResponse.builder()
                    .paymentDate(newDate)
                    .paymentNumber(i)
                    .amount(amount / terms).build()
            );
        }
        return listPayments;
    }

}
